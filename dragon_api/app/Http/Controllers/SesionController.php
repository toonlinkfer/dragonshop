<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use  App\Models\User;
use Illuminate\Support\Facades\Hash;
class SesionController extends Controller
{
    private $usuario;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'soy index';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $this->validate($request,[
                'nombre'=>'required',
                'correo'=>'required|email',
                'contrasena'=>'required',
                'tipo_cuenta'=>'required'
            ]);
            $this->usuario= new User();
            $this->usuario->name=$request->nombre;
            $this->usuario->correo=$request->correo;
            $this->usuario->contrasena=Hash::make($request->contrasena);
            $this->usuario->id_account_types_fk=$request->tipo_cuenta;
            $this->usuario->save();
            return "lo logre";        
        }catch(Exception $e){
            return "falle";
        }
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return 'soy store';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'soy show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return "soy el update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "soy el destructor";
    }

    public function pito(){
        
    }
}



/*public function registrarUsuario(){
    $usuario=new User();
    $usuario->name="roque";
    $usuario->correo="toonlinkfer@gmail.com";
    $usuario->contrasena="1234";
    $usuario->id_account_types_fk=1;
    try{
        if($usuario->save()){
            echo "no nepe";
        }else{
            echo "pito";
        }
    }catch(Exception $e){
        echo "pitote";
    }
  
}

public function iniciarSesion(){
    echo "soy el iniciar sesion";
}

public function recuperarContrasena(){
    echo "soy recuperar contraseña pito";
}

public function registrosCuentas(){

}
}
*/