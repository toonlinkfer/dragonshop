<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id("id_users_pk");
            $table->string('name');
            $table->string('correo')->unique();
            $table->timestamp('correo_verificado_at')->nullable();
            $table->string('contrasena');
            $table->bigInteger("id_account_types_fk")->foreign("id_account_types_fk")->constrained()->references("id_account_pk")->on("account_types")->onUpdate('cascade')->onDelete("cascade");
            $table->rememberToken();
            $table->string("api_token")->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
