<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('pruebaRegistro', [SesionController::class,"registrarUsuario"]);
Route::get('pruebaIniciarSesion', [SesionController::class,"iniciarSesion"]);
Route::get('pito', [SesionController::class,"registrarUsuario"]);*/
